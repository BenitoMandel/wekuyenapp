import json
import requests

def request_json(url):
  req = requests.get(url=url)
  return req.json()

api_key = "7oyzKcUc7IsouA9QOVoH1XnGccsoYdSr"

url_base = "http://dataservice.accuweather.com/"
url_location = url_base + "locations/v1/cities/geoposition/search?apikey={}&q={}%2C%20{}&language=en-US&details=false"
url_forecast = url_base + "forecasts/v1/daily/5day/{}?apikey={}&metric=true"

locations = {
  "quilpue": { "loc_key": '56787', "lat": -33.055, "lng": -71.435 },
  "valemana": { "loc_key": 0, "lat": -33.05, "lng": -71.39 },
  "limache": { "loc_key": 0, "lat": -33.00, "lng": -71.26 },
  "olmue": { "loc_key": 0, "lat": -33.00, "lng": -71.18 },
  "vina": { "loc_key": 0, "lat": -33.01, "lng": -71.55 },
  "concon": { "loc_key": 0, "lat": -32.93, "lng": -71.52 },
  "valpo": { "loc_key": 0, "lat": -33.04, "lng": -71.63 }
}

for loc in locations:
  loc_data = locations[loc]
  if  loc_data["loc_key"]> 0:
    loc_url = url_location.format(api_key, loc_data["lat"], loc_data["lng"])
    loc_json = requests.get(url=loc_url).json()
    locations[loc]["loc_key"] = loc_json["Key"]
    print(loc_json)
    exit()

location_data = requests.get(url=url_location.format(api_key, ))

print(url_forecast.format(quilpue[0], api_key))