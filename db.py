from flask_sqlalchemy import SQLAlchemy
from bcrypt import gensalt, hashpw

db = SQLAlchemy()

class Users(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    name = db.Column(db.Text)
    phone = db.Column(db.Text, unique=True)
    level = db.Column(db.Integer)
    pwd = db.Column(db.Text)

    def __init__(self, name, phone, lvl, password):
        self.name = name
        self.phone = phone
        self.level = lvl
        self.pwd = hashpw(bytes(password, 'utf-8'), gensalt())

    def __repr__(self):
        return "{}".format(self.name)


class Clients(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    name = db.Column(db.Text)
    address = db.Column(db.Text)
    route = db.Column(db.Text)
    # phone = db.Column(db.Text)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    mapcode = db.Column(db.Text)
    status = db.Column(db.Boolean)

    def __init__(self, name, route, address, lat, lng, mapcode, status):
        self.name = name
        self.route = route
        self.address = address
        self.lat = lat
        self.lng = lng
        self.mapcode = mapcode
        self.status = status

    def __repr__(self):
        return self.name


# class Path(db.Model):
#     _id = db.Column("id", db.Integer, primary_key=True)
#     origin = db.Column(db.Integer, db.ForeignKey('client.id'))
#     destin = db.Column(db.Integer, db.ForeignKey('cliend.id'))
#     distance = db.Column(db.Integer)
#     duration = db.Column(db.Integer)
