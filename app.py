from flask import Flask, url_for, render_template, redirect, request, session
from datetime import timedelta
from login import login_menu
from db import db, Users, Clients

app = Flask(__name__)
app.register_blueprint(login_menu, url_prefix="")
app.secret_key = "*[!_#¡?WEC:[WñeXC!~{"

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///wekuyen.sqlite3'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://flaskapp:flaskapi@localhost/wekuyen'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.permanent_session_lifetime = timedelta(hours=12)

db.init_app(app)

## DEFAULT ROUTING
@app.route('/')
def index():
    if "user" not in session:
        return redirect(url_for("login_menu.login"))
    else:
        return redirect(url_for("main"))


## MAIN MENU INTERFACE
@app.route("/main", methods=["GET", "POST"])
def main():
    if "user" in session:
        if session["level"] == 0:
            return render_template("admin.html", session=session)
        elif session["level"] == 1:
            return render_template("driver.html", session=session)
    else:
        return redirect(url_for("index"))

## RUN
if __name__ == "__main__":
    app.run(host='0.0.0.0', threaded=True, debug=True)
