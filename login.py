from flask import Blueprint, redirect, render_template, url_for, request, session, flash, current_app
from bcrypt import checkpw
from db import db, Users, Clients

login_menu = Blueprint("login_menu", __name__, static_folder='static', template_folder='templates')

def validate_user(usr, pss):
    user_from_db = Users.query.filter_by(name=usr).first()

    if user_from_db != None:
        return checkpw(bytes(pss, 'utf-8'), bytes(user_from_db.pwd, 'utf-8'))
    else:
        return False


def sorted_routes():
    routes = {
        'quilpue': { 'name': 'Quilpué' },
        'valemana': { 'name': 'Villa Alemana' },
        'vina': { 'name': 'Viña del Mar' },
        'concon': { 'name': 'Concón' },
        'limache': { 'name': 'Limache' },
        'olmue': { 'name': 'Olmué' }
    }

    def dict_client(cl):
        return {
            'name': cl.name,
            'route': cl.route,
            'lat': cl.lat,
            'lng': cl.lng
        }

    for route in routes:
        clients_objects = Clients.query.filter_by(route=routes[route]['name']).all()
        routes[route]['clients'] = [dict_client(cln) for cln in clients_objects]

    # Optimization left
    return routes

## LOGIN USER
@login_menu.route('/login', methods=["GET", "POST"])
def login():
    if "user" in session:
        return redirect(url_for("index"))

    if request.method == "POST":
        user = request.form["user"]
        pswd = request.form["password"]

        if validate_user(user, pswd):
            current_app.logger.info('Logged user: {}'.format(user))
            
            session.permanent = True
            session["user"] = user
            session["level"] = Users.query.filter_by(name=user).first().level
            session["routes"] = sorted_routes()

            return redirect(url_for("index"))
        else:
            flash('No te conozco')
            return render_template("login.html", session=session)
    else:
        return render_template("login.html", session=session)

@login_menu.route("/logout")
def logout():
    if "user" in session:
        session.pop("user", None)
        return redirect(url_for("login_menu.login"))
    else:
        return redirect(url_for("index"))

@login_menu.route('/createuser', methods=["GET", "POST"])
def createuser():

    if session["user"] != 'Admin':
        return redirect(url_for("index"))

    if request.method == "POST":
        name = request.form["user"]
        user_from_db = Users.query.filter_by(name=name).first()

        if user_from_db == None:
            pswd = request.form["password"]
            phone = request.form["phone"]
            new_usr = Users(name, phone, 0, pswd)
            db.session.add(new_usr)
            db.session.commit()

            return redirect(url_for("login_menu.login"))

        else:
            flash('Usuario existente')
            return render_template("login.html", session=session, signup=True)

    else:
        flash('!!')
        return render_template("login.html", session=session, signup=True)
    